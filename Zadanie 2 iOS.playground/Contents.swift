import AppKit
import PlaygroundSupport

func %(lhs: String, rhs: Int) -> Int {
    var result = 0
    lhs.forEach { (char) in
        result = result * 10 + Int(String(char))!
        result = result % rhs
    }
    return result
}

class BankAccount {
    var iban: String
    var number: String?
    var idBank: Int?
    var name: String?
    
    init?(iban: String) {
        self.iban = iban
        
        if !checkSum() {
            return nil
        }
        number = String(iban.suffix(26))
        (idBank, name) = findBankName(iban: iban)
        if (name == nil) {
            return nil
        }
    }
    
    func checkSum() -> Bool {
        // check the length of the number
        if iban.count != 28 {
            return false
        }
        
        // change of letters into numbers
        let letterToNumbers: [String: Int] = [
            "A": 10, "B": 11, "C": 12, "D": 13, "E": 14,
            "F": 15, "G": 16, "H": 17, "I": 18, "J": 19,
            "K": 20, "L": 21, "M": 22, "N": 23, "O": 24,
            "P": 25, "Q": 26, "R": 27, "S": 28, "T": 29,
            "U": 30, "V": 31, "W": 32, "X": 33, "Y": 34,
            "Z": 35
            ]
        
        let firstFourChar = String(iban.prefix(4))
        
        let firstTwoLetters = String(firstFourChar.prefix(2))
        let firstLetter = String(firstTwoLetters.prefix(1))
        let secondLetter = String(firstTwoLetters.suffix(1))
        
        let firstLetterAsNumber = letterToNumbers[firstLetter]
        let secondLetterAsNumber = letterToNumbers[secondLetter]
        let lettersAsNumber = "\(String(firstLetterAsNumber!))\(String(secondLetterAsNumber!))"
        
        //getting the first two digits
        let firstTwoDigits = String(firstFourChar.suffix(2))
        
        // moving the first 4 characters at the end of the number and changing the letters to previously prepared digits
        var numberToCheck = iban
        numberToCheck.removeFirst(4)
        numberToCheck.append("\(lettersAsNumber)\(firstTwoDigits)")
        
        // calculation of the remainder from division by 97, if the remainder is equal to 1, then the chcecksum is correct
        let score = numberToCheck % 97
        if score == 1 {
            return true
        } else {
            return false
        }
    }
    
    func findBankName(iban: String) -> (Int?, String?) {
        let listBanks: [Int: String] = [
            1010: "Narodowy Bank Polski",
            1020: "PKO BP",
            1030: "Bank Handlowy (Citi Handlowy)",
            1050: "ING",
            1060: "BPH",
            1090: "BZ WBK",
            1130: "BGK",
            1140: "mBank, Orange Finanse",
            1160: "Bank Millennium",
            1240: "Pekao",
            1280: "HSBC",
            1320: "Bank Pocztowy",
            1470: "Eurobank",
            1540: "BOŚ",
            1580: "Mercedes-Benz Bank Polska",
            1610: "SGB - Bank",
            1670: "RBS Bank (Polska)",
            1680: "Plus Bank",
            1750: "Raiffeisen Bank",
            1840: "Societe Generale",
            1870: "Nest Bank",
            1910: "Deutsche Bank Polska",
            1930: "Bank Polskiej Spółdzielczości",
            1940: "Credit Agricole Bank Polska",
            1950: "Idea Bank",
            2030: "BGŻ BNP Paribas",
            2070: "FCE Bank Polska",
            2120: "Santander Consumer Bank",
            2130: "Volkswagen Bank",
            2140: "Fiat Bank Polska",
            2160: "Toyota Bank",
            2190: "DnB Nord",
            2480: "Getin Noble Bank",
            2490: "Alior Bank, T-Mobile Usługi Bankowe"
        ]
        
        // separation of the bank identufier
        let start = iban.index(iban.startIndex, offsetBy: 4)
        let end = iban.index(iban.startIndex, offsetBy: 8)
        let range = start..<end
        let ID = Int(iban[range])
        
        var nameBank: String?
        
        if let id = ID {
            nameBank = listBanks[id]
        }
        
        return (ID, nameBank)
    }
}

let ibanOk = BankAccount(iban: "PL83101010230000261395100000")
let ibanNoBank = BankAccount(iban: "PL73101010230000261395100000")

