//: A Cocoa based Playground to present user interface

import AppKit
import PlaygroundSupport

let nibFile = NSNib.Name("MyView")
var topLevelObjects : NSArray?

Bundle.main.loadNibNamed(nibFile, owner:nil, topLevelObjects: &topLevelObjects)
let views = (topLevelObjects as! Array<Any>).filter { $0 is NSView }

// Present the view in Playground
PlaygroundPage.current.liveView = views[0] as! NSView

class ConferenceRoom {
    enum State: Equatable {
        case free
        case reserved(Int)  // time of booking a room in minute
    }
    
    var name: String        // name of conference room
    var state: State        // occupancy status
    
    init(name: String, state: State = .free) {
        self.name = name
        self.state = state
    }
    
    // Description of conference room
    func description() -> String {
        switch self.state {
            case .free:
                return "\(self.name)-free"
            case .reserved(let time):
                return "\(self.name)-reserved-\(time)"
        }
    }
}

// Addition of sample conference room
let colectionRoom = [
    ConferenceRoom(name: "1"),
    ConferenceRoom(name: "2"),
    ConferenceRoom(name: "3", state: .reserved(30)),
    ConferenceRoom(name: "4"),
    ConferenceRoom(name: "5", state: .reserved(4*60)),
    ConferenceRoom(name: "6"),
    ConferenceRoom(name: "7"),
    ConferenceRoom(name: "8", state: .reserved(8*60)),
    ConferenceRoom(name: "9"),
    ConferenceRoom(name: "10")
]

// The division of room into free and reserved
let freeRoom = colectionRoom.filter { $0.state == .free }
let reservedRoom = colectionRoom.filter { $0.state != .free }

// Display of devided room
freeRoom.forEach{ print($0.description()) }
reservedRoom.forEach{ print($0.description()) }
