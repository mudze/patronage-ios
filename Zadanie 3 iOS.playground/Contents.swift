import UIKit

struct userData: Codable {
    enum CodingKeys: String, CodingKey {
        case userData
    }
    enum userDataCodingKeys: String, CodingKey {
        case userName
        case userId
        case bankAccount
    }
    enum bankAccountCodingKeys: String, CodingKey {
        case accountNumber
        case bankId
        case bankName
    }
    
    let userName: String
    let userId: Int
    let accountNumber: String
    let id: String
    let name: String
    
    func encode(to encoder: Encoder) throws {
        var containerUserData = encoder.container(keyedBy: CodingKeys.self)
        var userData = containerUserData.nestedContainer(keyedBy: userDataCodingKeys.self, forKey: .userData)
        
        try userData.encode(userName, forKey: .userName)
        try userData.encode(userId, forKey: .userId)
        
        var bankAccountData = userData.nestedContainer(keyedBy: bankAccountCodingKeys.self, forKey: .bankAccount)
        
        try bankAccountData.encode(accountNumber, forKey: .accountNumber)
        try bankAccountData.encode(id, forKey: .bankId)
        try bankAccountData.encode(name, forKey: .bankName)
    }
    
    public init(from decoder: Decoder) throws {
        let containerUserData = try decoder.container(keyedBy: CodingKeys.self)
        let userData = try containerUserData.nestedContainer(keyedBy: userDataCodingKeys.self, forKey: .userData)
        
        userName = try userData.decode(String.self, forKey: .userName)
        userId = try userData.decode(Int.self, forKey: .userId)
        
        let bankAccountData = try userData.nestedContainer(keyedBy: bankAccountCodingKeys.self, forKey: .bankAccount)
        
        accountNumber = try bankAccountData.decode(String.self, forKey: .accountNumber)
        id = try bankAccountData.decode(String.self, forKey: .bankId)
        name = try bankAccountData.decode(String.self , forKey: .bankName)
    }
}

let jsonObj: [String: Any] = [
    "userData": [
        "userName": "NameExample",
        "userId": 123456,
        "bankAccount": [
            "accountNumber": "12341234",
            "bankId": "ExampleId",
            "bankName": "ExampleName"
        ]
    ]
]

let valid = JSONSerialization.isValidJSONObject(jsonObj)
let jsonData = try JSONSerialization.data(withJSONObject: jsonObj)

let decoder = JSONDecoder()
let decodeJSON = try decoder.decode(userData.self, from: jsonData)

let encoder = JSONEncoder()
let encodeJSON = try encoder.encode(decodeJSON)

let decodeEncodeJSON = try decoder.decode(userData.self, from: encodeJSON)


